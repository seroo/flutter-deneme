import 'package:flutter/material.dart';

//void main() {
//  runApp(MyApp());
//}

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MyAppState();
  }
}

// underscore koyunca sadece main dart dosyasından ulaşılabilir oluyor.
class _MyAppState extends State<MyApp> {

  var _questionIndex = 0;

  var questions = [
    'What is your favorite color',
    'What is your favorite movie',
  ];
  
  void _answerQuestion(arg) {
    print('Answer chosen #' + arg);
    setState(() {
      _questionIndex++;
    });

  }
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
      appBar: AppBar(
        title: Text('My first App'),
      ),
      body: Column(children: [
        Text(questions[_questionIndex]),
        RaisedButton(child: Text('Answer 1'),
        onPressed: () => _answerQuestion('1'),),
        RaisedButton(child: Text('Answer 2'),
        onPressed: () => _answerQuestion('2'),),
        RaisedButton(child: Text('Answer 3'),
          onPressed: () => print('Answer 3 '),),
      ],),
    ));
  }
}
